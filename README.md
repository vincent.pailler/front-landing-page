# Beyond-SI Landing page

Built thank to bootstrap template from [**startbootstrap.com**](https://startbootstrap.com/theme/landing-page) ! 

[MIT License applied](https://github.com/startbootstrap/startbootstrap-landing-page/blob/master/LICENSE).

![landingpage-screenshot](readme_img/landingpage-screenshot.png)

## Deployment and Maintenance

To deploy this small web site, please use the following repository [beyond-si](https://forgemia.inra.fr/beyond/beyond_si)


